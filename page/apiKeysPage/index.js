const { I } = inject()
const customMethod = require("../../utils/customMethod")
const apiKeysPageLocator = require('./locator')
module.exports = {
viewAPIKeysPage() {
    customMethod.clickElement(apiKeysPageLocator.setupTab)
    customMethod.clickElement(apiKeysPageLocator.apiKeys)
},
CreateAPIKey() {
    customMethod.clickElement(apiKeysPageLocator.createAPI)
}
}